$(document).ready(function(){
  $("#mycarousel").carousel( { interval: 2000 } );
  $("#carouselButton").click(function(){
      if($("#carouselButton").children('span').hasClass('fa-pause')){
          $("#mycarousel").carousel('pause');
          $('#carouselButton').removeClass('btn-danger').addClass('btn-success');
          $("#carouselButton").children('span').removeClass('fa-pause').addClass('fa-play');
      }
      else if($("#carouselButton").children('span').hasClass('fa-play')){
          $('#mycarousel').carousel('cycle');
          $('#carouselButton').removeClass('btn-success').addClass('btn-danger');
          $('#carouselButton').children('span').removeClass('fa-play').addClass('fa-pause');
      }    
  });
  $('#login').click(function(){
      $('#loginModal').modal();
  });
  $('.close').click(function(){
      $('.modal').modal('hide');
  });
  $('#reserveTableBtn').click(function(){
      $('#reserveModal').modal();
  });
  $('.cancel').click(function(){
      $('.modal').modal('hide');
  });
});